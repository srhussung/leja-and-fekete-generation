import sys

from itertools import product 

import numpy as np
from math import sqrt

sys.path.append('./convex_body')
from multi_index_generation import multi_index_generator
from grevlex import lexico
from polynomial import Polynomial

def polynomial_generator(n = 2, convex_body="Lp ball", p = 2, weights = [], convex_hull_points = [], fractional = True):
    """
    This function will output an ordered polynomial basis with respect to the specified convex body.

        n - dimension, > 1

        convex_body - Type of convex body. 
            Must be in ["Lp ball", "convex hull"]
            Only use weights for the Lp ball
            Only use convex_hull_points for the convex_hull option, don't use weights

        fractional - whether or not the degree is returned as a decimal or integer
            Must be True or False
            True means that the decimal degree is returned. False will round all answers.

        p - exponent for Lp ball. Must be positive, but can be less than 1.

        weights - allows for Lp ball or simplex that hits at different points. 
            E.g., a weighting of (2, 1, 3) will cause the convex body to touch points (2, 0, 0), (0, 1, 0), and (0, 0, 3)

        convex_hull_points - points for the convex hull. There can be any number of these               TODO Convex Hull and SImplex should automatically split

    """
    
    multi_index_gen = multi_index_generator(n = n, convex_body = convex_body, convex_hull_points = convex_hull_points, fractional=fractional)

    while True:
        yield Polynomial(1.0, n = n, mono=True, monomial_position = next(multi_index_gen))


if __name__ == "__main__":
    poly_gen = polynomial_generator(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [2, 1], [2, 2], [1, 3], [0, 3]], fractional=False)

    for i in range(10):
        print(next(poly_gen))

