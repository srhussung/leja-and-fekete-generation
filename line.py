
"""
This program generates a line point grid within the unit square in C

if just one: N  -- generates a line with N points
"""

import sys

def generate(N):
    args = sys.argv

    if len(args) == 2:
        N = int(args[1])

    return [[-1.0 + 2*i/(N-1)] for i in range(N)]

if __name__ == "__main__":
    for point in generate(10):
        print(point)
