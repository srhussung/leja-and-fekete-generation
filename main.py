#! /home/sthussun/intelpython3/bin/python3.6 
"""
This reads in a file of point locations, such as points.in, which can be specified

Then based on command line args it receives, it will compute Fekete, Leja, or in-between Points.

How many points? nested or not? These are all necessary to answer. 
"""

#Imports
import numpy as np
import random
import matplotlib.pyplot as plt
import argparse
import itertools
from matplotlib import colors

#Mine
from supplemental import *
import line

#Begin program

#Load Args
args = parse_args()
NCP = args.NCP
NLP = args.NLP
dimension = args.dimension
is_complex = args.is_complex

#Read in points
#cand_points = readpoints("points.in")
cand_points = line.generate(NCP)

#Create basis
max_degree = NLP
monomials = []
polynomials = []
poly_gen = generate_basis(points=cand_points)
for i in range(max_degree):
    (mono_temp, poly_temp) = next(poly_gen)
    monomials.append(mono_temp)
    polynomials.append(poly_temp)

##Test poly p 
#p = polynomials[3]
#for z in cand_points:
#    print(z[0], eval_poly(p, z, monomials))

#Random
select_points = random.sample(cand_points, len(monomials))

#N random draws: Requires cand_points, n_draws, monomials, polynomials
#n_random_draws(cand_points, 1000, monomials, polynomials)

#Choose next max_degree points
chosen_points = []
leja_block(monomials, polynomials, cand_points, chosen_points, max_degree)
#print(chosen_points)
plt.plot(chosen_points, np.zeros(len(chosen_points)), marker="o")
plt.show()

##What if we want to go back and add more?
#extend_by = 10
#for i in range(1, extend_by):
#
#    (mono_temp, poly_temp) = next(poly_gen)
#    monomials.append(mono_temp)
#    polynomials.append(poly_temp)
#
#    leja_block(monomials, polynomials, cand_points, chosen_points, 1)
#    for p in chosen_points:
#        print(p[0], i)
