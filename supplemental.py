import numpy as np
import numpy.linalg as npla
import random
import argparse

import matplotlib.pyplot as plt
from matplotlib import colors

from itertools import combinations

def parse_args():
    parser = argparse.ArgumentParser(description="Generate Leja and Fekete Points")
    parser.add_argument('--NCP', dest='NCP', type=int, default=100, help="Number of Candidate Points")
    parser.add_argument('--NLP', dest='NLP', type=int, default=10, help="Number of Leja Points")
    parser.add_argument('--dim', dest='dimension', type=int, default=1, help="Dimension of Space")
    parser.add_argument('--complex', dest='is_complex', type=bool, default=False, help="Whether or not the space is Complex. True = Complex, False = Real.")
    return parser.parse_args()

def readpoints(filename):
    """
    Gets points from file
    """
    cand_points = []
    with open(filename, "r") as f:
        for line in f:
            s = line.strip().split()
            cand_points.append(list(map(float, s)))

    return cand_points

def generate_basis(points=[]):
    """
    Generates the polynomial basis. 

    Can add more options later, such as 
        dimension
        complexity
        chebyshev polynomials
        orthonormalization
        convex bodies

    A polynomial is two parts.
    First, a list of degree tuples specifying monomials
    Second, a list of coefficients

    These are to be evaluted
    p(z_1, z_2) = sum(
    """

    #Determine whether or not to orthogonalize (This is an implied boolean.)
    orthogonalize = len(points) > 0

    i = 0
    monomials = []
    polynomials = []
    while True:
        monomials.append(i)

        #Each polynomial is a list of tuples, (ith monomial, coefficient)
        polynomials.append([(i, 1.0)])

        if(orthogonalize):
            pass

        yield [monomials[-1], polynomials[-1]]

        i = i + 1
    
def eval_poly(p, z, monomials):
    """
    For now, only one z value at a time
    """

    # For future, when you want multi-evaluation
    #Z = np.array(z)
    ##Convert Z to an array
    #Z = Z if len(Z.shape) > 1 else np.array([z])
    #print(Z)

    total = 0.0
    for (i, alpha) in p:
        total += alpha*z[0]**(monomials[i])
    return total

def Vandermonde(monomials, polynomials, points):
    """
    Minor storage could speed this up a lot
    """

    #We avoid overfilling with polys
    V = np.array([[eval_poly(p, z, monomials) for p in polynomials[:len(points)]] for z in points])

    return (V, abs(npla.det(V)))

def n_random_draws(cand_points, n_draws, monomials, polynomials):
    current_max = 0.0
    Maxes = [current_max]
    Draws = []
    for i in range(n_draws):
        select_points = random.sample(cand_points, len(monomials))
        V = Vandermonde(monomials, polynomials, select_points)[1]
        Draws.append(V)

        current_max = current_max if V < current_max else V
        #print(V)
        #print(current_max)
        Maxes.append(current_max)

    #plt.plot(Maxes)
    #plt.plot(Draws)
    plt.hist(Draws, bins=20)
    plt.yscale('log', nonposy='clip')
    plt.show()

def leja_block(monomials, polynomials, cand_points, chosen_points, n_to_choose):
    for n in range(n_to_choose):
        Dets = np.array([Vandermonde(monomials, polynomials, chosen_points + [point])[1] for point in cand_points])
        best_i = np.argmax(Dets)
        chosen_points.append(cand_points[best_i])

def fekete_block(monomials, polynomials, pre_cand_points, chosen_points, n_to_choose):
    #For Fekete blocks, it is helpful to remove any chosen_points from candidate points
    cand_points = clean(pre_cand_points, chosen_points)
    print(cand_points)
    exit()

    for n in range(n_to_choose):
        Dets = np.array([Vandermonde(monomials, polynomials, chosen_points + [point])[1] for point in cand_points])
        best_i = np.argmax(Dets)
        chosen_points.append(cand_points[best_i])

def clean(full_list, to_remove):
    return filter(lambda k : k not in to_remove, full_list)
