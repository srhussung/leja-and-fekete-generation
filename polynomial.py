import sys

from itertools import product 

import numpy as np
from math import sqrt

sys.path.append('./convex_body')
from grevlex import lexico

def get_nonzero_exp(p):
    p_ranges = [range(x) for x in list(p.coefs.shape)]
    p_exponents = list(product(*p_ranges))

    nonzero_p_exponents = []
    for exp in p_exponents:
        if p.coefs[exp] != 0:
            nonzero_p_exponents.append(exp)
    return nonzero_p_exponents


class Polynomial:
    def __init__(p, coefs, n=2, mono=False, monomial_position=[]):

        p.n = n

        if mono:
            #In this case, we have just the positional information
            monomial_pos_plus = [a + 1 for a in monomial_position]
            monomial_position = [[a] for a in monomial_position]
            A = np.zeros(shape=monomial_pos_plus)
            A[monomial_position] = coefs
            p.coefs = np.array(A)
        else:
            p.coefs = coefs

    def toString(p):
        #Advanced
        string = ""
        nonzero_exp = get_nonzero_exp(p)
        first = True
        for exp in nonzero_exp:
            if not first:
                string = string + "+ "
            if p.coefs[exp]!= 1.0 or sum(exp) == 0:
                string = string + str(p.coefs[exp]) + " "
            for i,n in enumerate(exp):
                if n != 0:
                    string = string + "z_" + str(i) + "^" + str(n) + " "
            first = False

        #string = string + "\n"
        return string

    def __str__(p):
        return p.toString()

    def __add__(p, q):
        #If you want to have constant addition automatically interpreted, do so here

        #Check for shape match, if none, we will have to rectify
        if p.coefs.shape != q.coefs.shape:
            p_shape = p.coefs.shape
            q_shape = q.coefs.shape
            max_shape = np.array([max(p_axis, q_axis) for p_axis,q_axis in zip(p_shape, q_shape)])

            p_shape = np.array(p_shape)
            q_shape = np.array(q_shape)

            p_diff = np.subtract(max_shape,p_shape)
            p_diff = tuple((0,d) for d in p_diff)
            q_diff = np.subtract(max_shape,q_shape)
            q_diff = tuple((0,d) for d in q_diff)

            new_p_coefs = np.pad(p.coefs, p_diff, 'constant', constant_values=0.0)
            new_q_coefs = np.pad(q.coefs, q_diff, 'constant', constant_values=0.0)

            new_coefs = new_p_coefs + new_q_coefs
        else:
            new_coefs = p.coefs + q.coefs

        #Trim if last rows contain only zeros.
        #TODO

        return Polynomial(new_coefs, n=p.n)

    def __mul__(p, q):
        if type(q) == type(p):
            p_nonzero_exp = get_nonzero_exp(p)
            q_nonzero_exp = get_nonzero_exp(q)

            polys_to_add = []
            for p_exp, q_exp in product(p_nonzero_exp, q_nonzero_exp):
                p_coef = p.coefs[p_exp]
                q_coef = q.coefs[q_exp]
                sum_position = np.sum([np.array(p_exp), np.array(q_exp)], axis = 0)
                polys_to_add.append(Polynomial(p_coef*q_coef, n=p.n, mono=True, monomial_position = sum_position))

            return np.sum(polys_to_add)
            
        else:
            #q is a scalar
            return Polynomial(q*p.coefs, n=p.n)

    def __rmul__(p, q):
        return p*q

    def __truediv__(p, alpha):
        return (1.0/alpha)*p

    def __sub__(p, q):
        return p + (-1)*q
        
    def __call__(p, point):
        """
        This method does assume that we are given a single point. Generalize later and recurse.
        Further, this is a *dense* evaluation. If you think you need more sparse evaluations, this
        might not work very well.
        """
        point = np.array(point)

        #If this is slowing things down, try precomputing this part
        ranges = [range(x) for x in list(p.coefs.shape)]
        exponents = product(*ranges)
        
        total = 0
        for exponent in exponents:
            total += p.coefs[exponent]*np.prod(np.power(point, exponent))

        return total

if __name__ == "__main__":
    mp = [2, 4]
    p = Polynomial(2.0, n = 2, mono=True, monomial_position = mp)
    mp = [3, 2]
    q = Polynomial(1.0, n = 2, mono=True, monomial_position = mp)
    mp = [1, 1]
    r = Polynomial(3.0, n = 2, mono=True, monomial_position = mp)
    
    print(p, " + ", q, " + ", r, " = ")
    print(p + q + r)
    print("---")
    print("2.0*", p, "=")
    print(2.0*p)
    print("---")
    print(p, "/2.0 = ")
    print(p/2.0)
    print("---")
    print("(", p, " + ", q, ")* ", r, " = ")
    print((p + q)*r)
    
