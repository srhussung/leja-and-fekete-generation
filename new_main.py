import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npla
import scipy.linalg as spla
from itertools import product

from generate_polynomials import polynomial_generator

#Construct Points
N = 50
xpoints = [-1.0 + 2.0*i/(N-1) for i in range(N)]
ypoints = [-1.0j + 2.0j*i/(N-1) for i in range(N)]
prepoints = np.array(list(product(xpoints,ypoints)))
points = np.array([sum(p) for p in prepoints])

display_points = list(product(xpoints,[np.real(p) for p in ypoints]))

#plt.scatter(*zip(*points))
#plt.show()

#Construct Polynomials
M = 15
polys = []
#poly_gen = polynomial_generator(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [2, 1], [2, 2], [1, 3], [0, 3]], fractional=False)
#poly_gen = polynomial_generator(n = 2, convex_body = "Lp ball", fractional=False)
poly_gen = polynomial_generator(n = 1, convex_body = "Lp ball", fractional=False)
for i in range(M):
    polys.append(next(poly_gen))

print("polys: ")
for p in polys:
    print(p)


V = np.array([[p(z) for z in points] for p in polys])

#Find AFP points
refine_iterations = 2
for i in range(refine_iterations + 1): #A la Vianello

    if(i > 0):
        V = V.T
        Q,R = spla.qr(V, mode='economic') 
        print("V:", V.shape)
        print("Q:", Q.shape)
        print("R:", R.shape)
        #U = npla.inv(R)
        V2 = npla.solve(R.T,V.T).T
        #V3 = npla.lstsq(R.T,V.T)[0].T #This one causing problems
        #V = np.matmul(V, U)
        #print("VvsV2:")
        #print(np.max(V-V2))
        #print("VvsV3:")
        #print(np.max(V-V3))
        V = V2
        V = V.T

    Q,R,P = spla.qr(V, pivoting=True) 
    indices = P[:M]
    square_V = V[:,indices]
    print("Current determinant: ", abs(npla.det(square_V)))
    print("Current mat:", square_V)
    selected_points = points[indices]

    #Find DLP points
    #TODO

    #Display
    X = [p.real for p in selected_points]
    Y = [p.imag for p in selected_points]
    plt.scatter(X,Y)
    plt.show()
