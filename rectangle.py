
"""
This program generates a rectangle point grid within the unit square in C

Two arguments can be given.

if just one: N  -- generates an N x N grid
if two: N and M -- generates an N x M grid
"""

import sys

args = sys.argv

if len(args) == 2:
    N = int(args[1])
    M = N
elif len(args) == 3:
    N = int(args[1])
    M = int(args[2])
else:
    N = M = 100

for i in range(N):
    for j in range(M):
        x = i/(N-1)
        y = j/(M-1)
        print(x, y)
        #z = x+y*1.0j
        #print(z)
