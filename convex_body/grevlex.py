
def lexico(a, b):
    """
    This function accepts to list-like objects of the same lenght, then does a lexicographic comparison.
    This means that it will return 1 if a is bigger, -1 if b is bigger, 0 if they are entirely equal.

    Use as a comparator function for sorting.
    """

    if len(a) != len(b):
        print("arrays of different length passed to grevlex.py: returning")
        return -10

    if a == b:
        return 0

    i = 0
    while a[i] == b[i]:
        i += 1

    if a[i] < b[i]:
        return 1
    elif a[i] > b[i]:
        return -1
    else:
        print("An error has occurred in grevlex.py")
        return -11


if __name__ == "__main__":
    print("grevlex([1, 1, 2], [1, 1, 3])")
    print(grevlex([1, 1, 2], [1, 1, 3]))
    print("grevlex([2, 1, 2], [1, 1, 3])")
    print(grevlex([2, 1, 2], [1, 1, 3]))
    print("grevlex([2, 1,], [2, 1, 3])")
    print(grevlex([2, 1,], [2, 1, 3]))
