import math
import numpy as np
import numpy.linalg as npla
from itertools import product

def bounding_box(k, n = 2, convex_body="Lp ball", weights = [], convex_hull_points = [], fractional = True):
    """
        This function will return the dimensions of a bounding box for the given degree

        - - - - - Required - - - - - 

        k

        - - - - - Optional - - - - - 

        n - dimension, > 1

        convex_body - Type of convex body. 
            Must be in ["Lp ball", "convex hull"]
            Only use weights for the Lp ball
            Only use convex_hull_points for the convex_hull option, don't use weights

        weights - allows for Lp ball or simplex that hits at different points. 
            E.g., a weighting of (2, 1, 3) will cause the convex body to touch points (2, 0, 0), (0, 1, 0), and (0, 0, 3)

        convex_hull_points - points for the convex hull. There can be any number of these               
    """

    #Find dimension of bounding square
    if len(weights) == 0:
        weights = np.ones(n)

    if len(convex_hull_points) == 0:
        convex_hull_points = np.ones(n)

    if convex_body == "Lp ball":
        size =  math.ceil(k*max(weights))
    elif convex_body == "convex hull":
        largest = max([sum(point) for point in convex_hull_points])
        size =  math.ceil(k*largest)
    else:
        print("Invalid choice of convex_body in bounding_box.py")
        return -1

    #Generate points
    lattice = []
    for d in range(n):
        lattice.append(list(range(0, size+1)))
    points = list(product(*lattice))
    return points
    


if __name__ == "__main__":
    print(bounding_box(1, n = 3, convex_body="Lp ball", weights = [1.0, 2.0, 1.0], convex_hull_points = [], fractional = True))
    print(bounding_box(1, n = 2, convex_body="convex hull", convex_hull_points = [[1, 0], [1, 2], [0, 1]], fractional = True))
