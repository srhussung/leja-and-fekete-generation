import math
import numpy as np
import numpy.linalg as npla
from itertools import combinations

def generate_degree_function(n = 2, convex_body="Lp ball", p = 2, weights = [], convex_hull_points = [], fractional = True):
    """
        This function will *return* a function to evaluate the degree of a given multi-index

        - - - - - Required - - - - - 

        n - dimension, > 1

        convex_body - Type of convex body. 
            Must be in ["Lp ball", "convex hull"]
            Only use weights for the Lp ball
            Only use convex_hull_points for the convex_hull option, don't use weights

        fractional - whether or not the degree is returned as a decimal or integer
            Must be True or False
            True means that the decimal degree is returned. False will round all answers.

        p - exponent for Lp ball. Must be positive, but can be less than 1.

        weights - allows for Lp ball or simplex that hits at different points. 
            E.g., a weighting of (2, 1, 3) will cause the convex body to touch points (2, 0, 0), (0, 1, 0), and (0, 0, 3)

        convex_hull_points - points for the convex hull. There can be any number of these               TODO Convex Hull and SImplex should automatically split

        The returned function will require a multi-index only, of the appropriate length.
        Ex: if constructed with n = 3 

            c_degree([4, 1, 2])

            is valid
    """

    if len(weights) == 0:
        weights = np.ones(n)

    if len(convex_hull_points) == 0:
        convex_hull_points = np.ones(n)

    if convex_body == "Lp ball":
        def c_degree(alpha):
            if len(alpha) != n:
                print("Error, multi-index of wrong length. Expected", n, "Recieved, ", len(alpha))
                return -1

            if type(p) == type("a"):
                if p.lower() in ["inf", "infty", "infinity"]:
                    return max(np.divide(alpha,weights))
            elif p > 0:
                return sum(np.power(np.divide(alpha,weights), p))**(1/p)
            else:
                print("Value of p is invalid")
                return -1

    elif convex_body == "convex hull":
        points = convex_hull_points
        if [0,0] in points:
            points.remove([0,0])

        if len(points) == n:
            #Simplex Case
            A = np.array(points)
            b = np.ones(len(points))
            coefs = npla.solve(A, b)

            def c_degree(alpha):
                if len(alpha) != n:
                    print("Error, multi-index of wrong length. Expected", n, "Recieved, ", len(alpha))
                    return -1
                return np.dot(alpha, coefs)

        elif len(points) > n:
            #Convex Hull case: will need to recurse
            combos = list(combinations(points, n))
            simplex_degrees = [generate_degree_function(n = n, convex_body="convex hull", convex_hull_points = combo_points) for combo_points in combos]

            #Remove ''inner faces"
            simplex_degrees_at_points = [[degree(point) for point in points] for degree in simplex_degrees]

            #Manual
            outer_simplex_degrees = []
            for degree in simplex_degrees:
                simplex_degree_at_points = np.array([degree(point) for point in points])
                if all(simplex_degree_at_points <= 1.0):
                    outer_simplex_degrees.append(degree)

            def c_degree(alpha):
                if len(alpha) != n:
                    print("Error, multi-index of wrong length. Expected", n, "Recieved, ", len(alpha))
                    return -1
                return max([simplex_deg(alpha) for simplex_deg in outer_simplex_degrees])
        else:
            print("Not enough points to create simplex. (We have removed 0,0 from the list.)")

    if fractional:
        return c_degree
    else:
        def round_c_degree(alpha):
            return math.ceil(c_degree(alpha))
        return round_c_degree


if __name__ == "__main__":
    #degree = generate_degree_function(p = 1)
    #degree = generate_degree_function(p = 1, fractional=False)
    #degree = generate_degree_function(p = 2, weights = [2, 3])
    #degree = generate_degree_function(p = "inf", weights = [2, 3])
    #degree = generate_degree_function(p = "inf", weights = [2, 3], fractional = False)
    #degree = generate_degree_function(p = 1/2, weights = [2, 3], fractional = False)
    #degree = generate_degree_function(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [0, 2], [2, 1]])
    #degree = generate_degree_function(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [0, 2], [2, 1]], fractional=False)
    degree = generate_degree_function(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [2, 1], [2, 2], [1, 3], [0, 3]], fractional=False)

    for b in [[1, 1], [0, 2], [3, 0], [2, 1], [1, 4]]:
        print("degree(", b, ") = ", degree(b))

