from itertools import chain
from functools import cmp_to_key

from generate_c_degree_function import generate_degree_function
from grevlex import lexico
from bounding_box import bounding_box


def multi_index_generator(n = 2, convex_body="Lp ball", p = 2, weights = [], convex_hull_points = [], fractional = True):
    """
        This function will yield multi-indices for polynomials relating to the

        - - - - - Required - - - - - 

        n - dimension, > 1

        convex_body - Type of convex body. 
            Must be in ["Lp ball", "convex hull"]
            Only use weights for the Lp ball
            Only use convex_hull_points for the convex_hull option, don't use weights

        fractional - whether or not the degree is returned as a decimal or integer
            Must be True or False
            True means that the decimal degree is returned. False will round all answers.

        p - exponent for Lp ball. Must be positive, but can be less than 1.

        weights - allows for Lp ball or simplex that hits at different points. 
            E.g., a weighting of (2, 1, 3) will cause the convex body to touch points (2, 0, 0), (0, 1, 0), and (0, 0, 3)

        convex_hull_points - points for the convex hull. There can be any number of these               TODO Convex Hull and SImplex should automatically split

        The returned function will require a multi-index only, of the appropriate length.
        Ex: if constructed with n = 3 

            c_degree([4, 1, 2])

            is valid
    """

    #Make degree function
    degree = generate_degree_function(n = n, convex_body=convex_body, p = p, weights = weights, convex_hull_points = convex_hull_points, fractional = fractional)
    std_degree  = generate_degree_function(n = n, convex_body="Lp ball", p = 1, fractional = False)

    queue = []
    used = []

    k = 0
    while True:
        new_indices = list(filter(lambda i : i not in used, bounding_box(k, n = n, convex_body=convex_body, weights = weights, convex_hull_points = convex_hull_points, fractional = fractional)))
        queue = queue + [(p, degree(p), std_degree(p)) for p in new_indices]
        used =  used + new_indices

        k_queue = list(filter(lambda p : p[1] == k, queue))
        queue = list(filter(lambda p : p not in k_queue, queue))

        #Iterate through grevlex orderings
        d = 0
        while len(k_queue) > 0:
            d_queue = list(filter(lambda p : p[2] == d, k_queue))
            k_queue = list(filter(lambda p : p not in d_queue, k_queue))
            d_queue = [p[0] for p in d_queue]

            d_queue = sorted(d_queue, key = cmp_to_key(lexico))
            while len(d_queue) > 0:
                yield d_queue.pop(0)
            d += 1
        
        k += 1

if __name__ == "__main__":
    """
    This is a graphical demo of the multiple index generation. It goes through two iterations, once with the L2 ball, then with a convex hull. 
    
    Hit enter to move to the next point, and type anything else before hitting enter to move to the next plot.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    from math import sqrt

    plt.ion()
    fig, ax = plt.subplots()
    #size = 13
    size = 6 
    plt.xlim(-0.1, size)
    plt.ylim(-0.1, size)

    #Draw circles
    x1 = []
    x2 = []
    for r in range(size*3):
        theta = np.linspace(0, 2*np.pi, 100)
        x1.append(r*np.cos(theta))
        x2.append(r*np.sin(theta))
        ax.plot(x1[-1], x2[-1], '-', color="black")

    multi_index_gen = multi_index_generator(n = 2, convex_body = "Lp ball", fractional=False)
    points = []
    instring = ""
    while instring == "":
        points.append(next(multi_index_gen))
        instring = input(points[-1])

        x, y = list(zip(*points))
        x = list(x)
        y = list(y)
        ax.plot(x, y, 'o', color="blue", marker=".")
        ax.annotate(len(x), (x[-1], y[-1]))
    
    #plt.clf()

    print("Readying next plot!")

    convex_hull_points = [[1, 0], [2, 1], [2, 2], [1, 3], [0, 3]]

    multi_index_gen = multi_index_generator(n = 2, convex_body = "convex hull", convex_hull_points = [[1, 0], [2, 1], [2, 2], [1, 3], [0, 3]], fractional=False)

    fig, ax = plt.subplots()
    #size = 13
    size = 8
    plt.xlim(-0.1, int(size))
    plt.ylim(-0.1, int(size))

    #Draw convex hulls
    x1, x2 = list(zip(*convex_hull_points))
    x1 = np.array(x1)
    x2 = np.array(x2)
    for r in range(size*3):
        x = x1*r
        y = x2*r
        ax.plot(x, y, '-', color="black")

    points = []
    instring = ""
    while instring == "":
        points.append(next(multi_index_gen))
        instring = input(points[-1])

        x, y = list(zip(*points))
        x = list(x)
        y = list(y)
        ax.plot(x, y, 'o', color="blue", marker=".")
        ax.annotate(len(x), (x[-1], y[-1]))
